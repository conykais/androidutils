package com.replacestring.string_script;

import org.junit.Test;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Main {

    private static final String BASE_PATH = "/Users/chenquangui/Downloads/反编译call/out";

    @Test
    public void deduplication() {
        //只需填文件名(不含后缀)
        String fileName = "firebase";
        //例 BASE_PATH/firebase.txt 输出 BASE_PATH/firebase2.txt
        Utils.deduplication(BASE_PATH, fileName);
    }

    @Test
    public void replaceSmaliFilePath() {
        //仅支持修改smali文件
        //例  :com.bestflash.ledflash.flashlight.BaseApplication.smali  ->  com.ledflash.pro.App.smali
        //输入com.bestflash.ledflash.flashlight.BaseApplication.smali 实际输出 com/ledflash/pro/App.smali
        Utils.replaceSmali(BASE_PATH, "com.bestflash.ledflash.flashlight.BaseApplication", "com.ledflash.pro.App");
    }

    @Test
    public void replaceSmaliPackagePath() {
        //仅支持修改smali包路径
        //例 :com.bestflash.ledflash.flashlight  ->  com.ledflash.pro
        //输入com.bestflash.ledflash.flashlight 实际输出 com/ledflash/pro/
        Utils.replacePackagePath(BASE_PATH, "com.bestflash.ledflash.flashlight", "com.ledflash.pro");
    }

    @Test
    public void replaceString(){
        //纯改文件内部的字符串 和 包名无关
        Map<String, String> constMapping = new HashMap<>();
        //替换smali路径 com/aaa/bbb/ccc -> com/ccc/ddd/eee
//        constMapping.put("com/aaa/bbb/ccc ", "com/ccc/ddd/eee");
        //替换java包名 com.aaa.bbb.ccc -> com.ccc.ddd.eee
//        constMapping.put("com.aaa.bbb.ccc", "com.ccc.ddd.eee");
        //App Name
//        constMapping.put("Antlers Wallpapers", "Aesthetic Wallpaper");
        //其他常量字符串
        constMapping.put("Color Call - Call Flash,Screen,LED Flashlight", "Call pro");
        Utils.replaceString(BASE_PATH, constMapping);
    }

    /**
     *
     * android 资源文件
     * 例 cqg_splash_bg.png -> abc_splash_bg.png
     *
     */
    @Test
    public void replaceResPrefixName() {
        File file = new File(BASE_PATH, "res/");
        Utils.replaceRes(file.getAbsolutePath(), file.listFiles(), Utils.ResType.DRAWABLE, "ccf", "cqg");
    }
}
