package com.replacestring.string_script;

import com.replacestring.string_script.res.engine.DrawableReplaceEngine;
import com.replacestring.string_script.res.engine.LayoutReplaceEngine;
import com.replacestring.string_script.res.engine.ReplaceEngine;
import com.replacestring.string_script.res.ResourcePath;
import com.replacestring.string_script.res.engine.StringReplaceEngine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    /**
     * @param basePath 操作目录
     * @param fileName 目标文件(不含文件后缀)
     */
    public static void deduplication(String basePath, String fileName) {
        String inName = String.format("%s.txt", fileName);
        String outName = String.format("%s2.txt", fileName);
        basePath = String.format(basePath + "/%s", inName);
        File file = new File(basePath);
        File outFile = new File(file.getParent(), outName);
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            FileOutputStream outputStream = new FileOutputStream(outFile);
            String line = null;
            Map<String, String> map = new HashMap<>();
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                if (!map.containsKey(line)) {
                    map.put(line, line);
                    stringBuilder.append(line);
                    stringBuilder.append("\n");
                }
            }
            bufferedReader.close();
            inputStreamReader.close();
            fileInputStream.close();
            outputStream.write(stringBuilder.toString().getBytes());
            outputStream.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param basePath          基础路径
     * @param originPackagePath 原本包路径
     * @param targetPackagePath 最终生成结果包路径
     */
    public static void replacePackagePath(String basePath, String originPackagePath, String targetPackagePath) {
        startProcessCode(basePath, originPackagePath, targetPackagePath, true);
    }

    /**
     * @param basePath                   基础路径
     * @param originSmaliFullPackageName 原本smali文件路径
     * @param targetSmaliFullPackageName 最终生成的smali文件路径
     */
    public static void replaceSmali(String basePath, String originSmaliFullPackageName, String targetSmaliFullPackageName) {
        startProcessCode(basePath, originSmaliFullPackageName, targetSmaliFullPackageName, false);
    }

    /**
     * @param packageName smali包路径或者文件路径
     * @param suffix      文件后缀 ".smali" 或 ""  [ 空包代表包路径目录; smali代表文件路径 ]
     * @return 例1 输入 aaa.bbb.ccc 返回值 aaa/bbb/ccc/ ; 例2 aaa.bbb.ccc.MainActivity.smali 返回值 aaa/bbb/ccc/MainActivity.smali
     */
    private static String getSMaliPath(String packageName, String suffix) {
        int block;
        StringBuilder format = new StringBuilder();
        String[] oldPackageNameBlock = packageName.split("\\.");
        block = oldPackageNameBlock.length;
        boolean isSmaliFile = suffix.equals(".smali");
        for (int i = 0; i < block; i++) {
            if (isSmaliFile && (i == (block - 1))) {
                format.append("%s");
                format.append(suffix);
            } else {
                format.append("%s/");
            }
        }
        return String.format(format.toString(), (Object[]) oldPackageNameBlock);
    }

    private static void startProcessCode(String basePath, String origin, String target, boolean path) {
        //noinspection UnnecessaryLocalVariable
        String oldPackageName = origin;
        //noinspection UnnecessaryLocalVariable
        String newPackageName = target;
        String suffix = path ? "" : ".smali";//改路径
        String oldPackagePath = getSMaliPath(oldPackageName, suffix);
        String newPackagePath = getSMaliPath(newPackageName, suffix);
        //替换文件路径和smali报名路径 com/aaa/bb/ccc
        //修改文件包名或者文件名
        replaceAllSMaliPackagePath(String.format(Locale.US, "%s/%s", basePath, "smali/").replaceAll("//", "/"), oldPackagePath, newPackagePath);
        //有的apk可能会有smali_classes2
        File file = new File(String.format(Locale.US, "%s/%s", basePath, "smali_classes2/").replaceAll("//", "/"));
        if (file.exists() && file.isDirectory()) {
            replaceAllSMaliPackagePath(file.getAbsolutePath(), oldPackagePath, newPackagePath);
        }
        //纯改文件内部的字符串 和 包名无关
        Map<String, String> constMapping = new HashMap<>();
        //替换smali路径 com/aaa/bbb/ccc -> com/ccc/ddd/eee
        constMapping.put(oldPackageName.replaceAll("\\.", "/"), newPackageName.replaceAll("\\.", "/"));
        //替换java包名 com.aaa.bbb.ccc -> com.ccc.ddd.eee
        constMapping.put(oldPackageName, newPackageName);
        replaceConstString(new File(basePath).listFiles(), constMapping);
    }

    public static void replaceString(String basePath, Map<String, String> mapping) {
        replaceConstString(new File(basePath).listFiles(), mapping);
    }

    private static void replaceConstString(File[] files, Map<String, String> constMapping) {
        String content;
        boolean isReplace = false;
        boolean firstPrint = true;
        if (!constMapping.isEmpty()) {
            if (files != null && files.length > 0) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        if (file.getAbsolutePath().endsWith("/.git")
                                || file.getAbsolutePath().endsWith("/build")
                                || file.getAbsolutePath().endsWith("/lib")
                                || file.getAbsolutePath().endsWith("/source")) {
                            continue;
                        }
                        replaceConstString(file.listFiles(), constMapping);
                    } else {
                        content = getContent(file);
                        if (content != null) {
                            Set<Map.Entry<String, String>> entries = constMapping.entrySet();
                            for (Map.Entry<String, String> entry : entries) {
                                if (content.contains(entry.getKey())) {
                                    if (firstPrint) {
                                        System.out.println(String.format(":%s", file.getAbsolutePath()));
                                        firstPrint = false;
                                    }
                                    content = content.replaceAll(entry.getKey(), entry.getValue());
                                    System.out.println(String.format(":[%s]->[%s]", entry.getKey(), entry.getValue()));
                                    isReplace = true;
                                }
                            }
                            if (isReplace) {
                                saveFile(content, file);
                            }
                            firstPrint = true;
                            isReplace = false;
                        }
                    }
                }
            }
        }
    }

    /**
     * 替换smali的文件路径和包路径
     * 例如com/aa/bb/cc
     */
    private static void replaceAllSMaliPackagePath(String baseFilePath, String basePackagePath, String targetPackagePath) {
        File baseFile = new File(baseFilePath);
        if (baseFile.exists() && baseFile.isDirectory()) {
            File[] files = baseFile.listFiles();
            copyAndReplaceSMaliPackagePath(basePackagePath, targetPackagePath, files);
        }
    }

    private static boolean copyAndReplaceSMaliPackagePath(String basePath, String targetPath, File[] files) {
        String outPath;
        File outFile;
        String content;
        boolean result = false;
        //此处文件路径并非真实路径,当前仅为方便得到文件名
        String baseFileName = new File(basePath).getName();
        Pattern pattern = Pattern.compile("\\w+(?<inner>\\$\\w)+.smali");
        if (files != null && files.length > 0) {
            for (File currentFile : files) {
                if (currentFile.isDirectory()) {
                    if (currentFile.getAbsolutePath().endsWith("/.git")
                            || currentFile.getAbsolutePath().endsWith("/build")
                            || currentFile.getAbsolutePath().endsWith("/source")) {
                        continue;
                    }
                    result = copyAndReplaceSMaliPackagePath(basePath, targetPath, currentFile.listFiles());
                    if (result) {
                        //noinspection ResultOfMethodCallIgnored
                        currentFile.delete();//删除目录
                    }
                } else {
                    outPath = currentFile.getAbsolutePath();
                    if (outPath.endsWith(".smali") && basePath.endsWith(".smali")
                            && targetPath.endsWith(".smali")) {
                        int index;
                        int index2;
                        boolean fullMatchSMali = false;
                        boolean matchPath = false;
                        String currentFileName;
                        String finalSourcePath = basePath;
                        String finalTargetPath = targetPath;
                        if (currentFile.getParentFile().getAbsolutePath().endsWith(new File("/", basePath).getParentFile().getAbsolutePath())) {
                            if (basePath.endsWith(".smali") && targetPath.endsWith(".smali")) {
                                String temp = baseFileName;
                                currentFileName = currentFile.getName();
                                index = currentFileName.indexOf(".smali");
                                boolean endsWith = false;
                                if (currentFileName.length() > temp.length()) {
                                    //temp > a.smali
                                    //currentFileName > bba.smali
                                    if (currentFileName.endsWith(temp)) {
                                        endsWith = true;
                                    }
                                }
                                if (!endsWith && index != -1) {
                                    index2 = temp.indexOf(".smali");
                                    if (index2 != -1) {
                                        currentFileName = currentFileName.substring(0, index);
                                        temp = temp.substring(0, index2);
                                        //完全相等
                                        if (currentFileName.equals(temp)) {
                                            fullMatchSMali = true;
                                            outPath = outPath.replace(basePath, targetPath);
                                        } else if (currentFileName.startsWith(String.format("%s%s", temp, "$"))
                                                || (currentFileName.startsWith(temp) && currentFileName.endsWith("_ViewBinding"))) {
                                            //模糊包含，有可能是内部类
                                            fullMatchSMali = true;
                                            //..../com/magic/noovacam/activities/ImageHandleActivity$1.smali
                                            //..../com/magic/noovacam/activities/ImageProcessActivity.smali
                                            String targetName;
                                            boolean innerClass = true;
                                            index = currentFileName.indexOf("$");
                                            if (index == -1) {
                                                innerClass = false;
                                                index = currentFileName.indexOf("_ViewBinding");
                                            }
                                            if (index != -1) {
                                                currentFileName = currentFileName.substring(0, index);
                                                //noinspection ConstantConditions
                                                if (currentFileName == null || currentFileName.length() == 0) {
                                                    continue;
                                                }
                                                index = targetPath.lastIndexOf("/");
                                                if (index != -1) {
                                                    index2 = targetPath.indexOf(".smali");
                                                    if (index2 != -1) {
                                                        // xxx/bbb/ccc/{ImageHandleActivity}.smali
                                                        targetName = targetPath.substring(index + 1, index2);
                                                        //noinspection ConstantConditions
                                                        if (targetName == null || targetName.length() == 0) {
                                                            continue;
                                                        }
                                                        //内部类名字窜改
                                                        //xx$1$2 [ xx->bb ] > bb$1$2
                                                        outPath = outPath.replace(currentFileName, targetName);
                                                        //输出路径窜改
                                                        outPath = outPath.replace(basePath.substring(0, basePath.lastIndexOf("/")), targetPath.substring(0, targetPath.lastIndexOf("/")));
                                                        String realCurrentFileName = currentFile.getName();

                                                        index = basePath.lastIndexOf("/");
                                                        if (index == -1) {
                                                            continue;
                                                        }
                                                        String suffix = "";
                                                        if (innerClass) {
                                                            Matcher matcher = pattern.matcher(realCurrentFileName);
                                                            if (matcher.find()) {
                                                                suffix = matcher.group("inner");
                                                            }
                                                        } else {
                                                            String[] arr = realCurrentFileName.split("\\.");
                                                            if (arr.length == 2) {
                                                                String name = arr[0];
                                                                if (name.length() > currentFileName.length()) {
                                                                    suffix = name.substring(currentFileName.length());
                                                                }
                                                            }
                                                        }
                                                        // /com/magic/noovacam/activities/ImageHandleActivity$1.smali
                                                        finalSourcePath = String.format(Locale.US, "%s%s%s", basePath.substring(0, index + 1), currentFileName, suffix);
                                                        index = targetPath.lastIndexOf("/");
                                                        if (index == -1) {
                                                            continue;
                                                        }
                                                        finalTargetPath = String.format(Locale.US, "%s%s%s", targetPath.substring(0, index + 1), targetName, suffix);
                                                    }
                                                }
                                            } else {
                                                continue;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (outPath.contains(basePath)) {
                                    matchPath = true;
                                    outPath = outPath.replace(basePath, targetPath);
                                }
                            }
                        }
                        if (fullMatchSMali || matchPath) {//路径匹配,smali匹配,这里篡改 文件名/文件路径/smali路径
                            //最终输出文件路径
                            outFile = new File(outPath);
                            content = getContent(currentFile);
                            if (content != null) {
                                //替换文件里面的内容
                                content = content.replaceAll(finalSourcePath, finalTargetPath);
                                result = saveFile(content, outFile);//保存新路径
                                //noinspection ResultOfMethodCallIgnored
                                currentFile.delete();//删除源文件
                                if (result) {
                                    System.out.println(String.format("replaceSMaliPackage:%s->%s", currentFile.getAbsolutePath(), outFile.getAbsolutePath()));
                                } else {
                                    System.err.println(String.format("replaceSMaliPackage:%s->error", currentFile.getAbsolutePath()));
                                }
                                result = true;
                            }
                        } else {//路径匹配 但文件不匹配 意思是这里要篡改文件内容 文件路径/smali路径
                            //批量修改所有有引用basePath的文件内容
                            content = getContent(currentFile);
                            if (content != null && content.contains(basePath)) {
                                //替换文件里面的内容
                                content = content.replaceAll(basePath, targetPath);
                                saveFile(content, currentFile);//覆盖原来的文件
                                System.out.println(String.format("replaceSMaliPackage:%s", currentFile.getAbsolutePath()));
                            }
                        }
                    } else if (outPath.endsWith(".smali") && basePath.endsWith("/") && targetPath.endsWith("/")) {
                        //批量修改文件路径(包名) 和 引用内容
                        content = getContent(currentFile);
                        if (content != null && currentFile.getAbsolutePath().contains(basePath)) {
                            //替换文件里面的内容
                            if (content.contains(basePath)) {
                                content = content.replaceAll(basePath, targetPath);
                            }
                            //修改输出路径
                            outFile = new File(currentFile.getAbsoluteFile().getAbsolutePath().replace(basePath, targetPath));
                            if (!outFile.exists()) {
                                result = saveFile(content, outFile);//覆盖原来的文件
                                if (result) {
                                    //noinspection ResultOfMethodCallIgnored
                                    currentFile.delete();
                                    System.out.println(String.format(":replace path:[%s]->[%s]", currentFile.getAbsolutePath(), outFile.getAbsolutePath()));
                                    System.out.println(String.format(":[%s]->[%s]", basePath, targetPath));
                                } else {
                                    System.err.println(String.format(":replace path error :[%s]", basePath));
                                }
                            } else {
                                System.err.println(String.format(":warring path exists :[%s]", outFile.getAbsolutePath()));
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    public static String getContent(File file) {
        try {
            if (file != null) {
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String line;
                StringBuilder stringBuilder = new StringBuilder();
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                    stringBuilder.append("\n");
                }
                bufferedReader.close();
                inputStreamReader.close();
                fileInputStream.close();
                return stringBuilder.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean saveFile(String content, File file) {
        try {
            File paren = file.getParentFile();
            if (!paren.exists()) {
                paren.mkdirs();
            }
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(content.getBytes());
            fileOutputStream.flush();
            return true;
        } catch (Throwable e) {

        }
        return false;
    }

    public static boolean LOG = false;

    public enum ResType {
        // ../res/drawable/
        // ../res/drawable-xxhdpi/
        // ../res/drawable-v19/
        // ../res/drawable-rtl/
        DRAWABLE("drawable", new DrawableReplaceEngine()),
        // ../res/mipmap/
        // ../res/mipmap-xxhdpi/
        // ../res/mipmap-v26/
        MIPMAP("mipmap", new DrawableReplaceEngine()),
        // ../res/values/
        // ../res/values-v21/
        // ../res/values-UK/
        // ../res/values-US/
        STRING("values", new StringReplaceEngine()),
        // ../res/layout/
        // ../res/layout-v19/
        // ../res/layout-rtl/
        // ../res/layout-night/
        LAYOUT("layout", new LayoutReplaceEngine()),
        NONE("none", null);

        private final static String PNG = ".png";
        private final static String JPG = ".jpg";
        private final static String XML = ".xml";

        private String mDirSuffix;
        private ReplaceEngine mEngine;

        public String getDirPrefix() {
            return mDirSuffix;
        }

        ResType(String mDirSuffix, ReplaceEngine mEngine) {
            this.mDirSuffix = mDirSuffix;
            this.mEngine = mEngine;
        }

        public static boolean isFile(File file, ResType type) {
            if (file != null && file.isFile() && file.exists()) {
                String name = file.getName().toLowerCase();
                switch (type) {
                    case DRAWABLE:
                    case MIPMAP:
                        if (name.endsWith(PNG) || name.endsWith(JPG)) {
                            return true;
                        }
                        break;
                    case LAYOUT:
                    case STRING:
                        if (name.endsWith(XML)) {
                            return true;
                        }
                        break;
                }

            }
            return false;
        }

        public static boolean isDir(File file, ResType type) {
            if (file != null && file.isDirectory() && file.exists()) {
                File[] files = file.listFiles();
                if (files == null || files.length == 0) {
                    return false;
                }
                String name = file.getName().toLowerCase();
                return name.startsWith(type.getDirPrefix());
            }
            return false;
        }

        private static final Pattern valuesPattern = Pattern.compile("values[\\w+]*");

        public static ResType getType(File file) {
            if (file != null) {
                if (isDir(file, DRAWABLE)) {
                    return DRAWABLE;
                }
                if (isDir(file, MIPMAP)) {
                    return MIPMAP;
                }
                if (file.isDirectory() && file.exists()) {
                    File[] files = file.listFiles();
                    if (files != null && files.length > 0) {
                        if (valuesPattern.matcher(file.getAbsolutePath()).find()) {
                            return STRING;
                        }
                    }
                }
                if (isDir(file, LAYOUT)) {
                    return LAYOUT;
                }
            }
            return NONE;
        }

        public void replace(ResourcePath resourcePath, String prefix, String targetPrefix) {
            if (mEngine != null) {
                mEngine.replacePrefix(resourcePath, prefix, targetPrefix);
            }
        }

        public static void findResReference(String res, String resType, List<File> files, Map<String, List<File>> referenceMap) {
            for (File drawableDir : files) {
                if (drawableDir.exists() && drawableDir.isDirectory()) {
                    File[] drawables = drawableDir.listFiles();
                    if (drawables != null && drawables.length > 0) {
                        for (File file : drawables) {
                            if (file.isFile() && file.getName().endsWith(".xml")) {
                                String content = getContent(file);
                                String reference = String.format("@%s/%s", resType, res);
                                if (content != null && content.contains(reference)) {
                                    if (!referenceMap.containsKey(res)) {
                                        List<File> references = new ArrayList<>();
                                        references.add(file);
                                        referenceMap.put(res, references);
                                    } else {
                                        referenceMap.get(res).add(file);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void replaceRes(String baseResPath, File[] files, ResType type, String prefix, String targetPrefix) throws IllegalArgumentException {
        if (type == null || type == ResType.NONE) {
            throw new IllegalArgumentException("type 仅支持 { drawable; strings; layout ;mipmap } !!!");
        }
        if (baseResPath == null || baseResPath.length() == 0) {
            throw new IllegalArgumentException("baseResPath 不能为空 !!!");
        }
        if (prefix.equals(targetPrefix)) {
            throw new IllegalArgumentException(String.format(Locale.US, "prefix { %s -> %s } 前后一致 !", prefix, targetPrefix));
        }
        String PNG = ".png";
        String JPG = ".jpg";
        String XML = ".xml";
        Pattern pngPattern = Pattern.compile(String.format(Locale.US, "%s_(?<name>\\w+)%s", prefix, PNG));
        Pattern jpgPattern = Pattern.compile(String.format(Locale.US, "%s_(?<name>\\w+)%s", prefix, JPG));
        Pattern xmlPattern = Pattern.compile(String.format(Locale.US, "%s_(?<name>\\w+)%s", prefix, XML));
        if (files != null && files.length > 0) {
            ResourcePath resourcePath = new ResourcePath();
            resourcePath.publicFile = new File(baseResPath, "values/public.xml");
            resourcePath.drawablesFile = new File(baseResPath, "values/drawables.xml");
            resourcePath.manifestFile = new File(new File(baseResPath).getParentFile(), "AndroidManifest.xml");
            for (File file : files) {
                //遍历得到所有drawable文件夹
                //遍历得到所有mipmap文件夹
                //遍历得到所有layout
                //../values/public.xml
                //../values/drawables.xml (如果有别名的话)
                switch (ResType.getType(file)) {
                    case DRAWABLE:
                        resourcePath.drawableDir.add(file);
                        break;
                    case MIPMAP:
                        resourcePath.mipmapDir.add(file);
                        break;
                    case STRING:
                        resourcePath.stringDir.add(file);
                        break;
                    case LAYOUT:
                        resourcePath.layoutDir.add(file);
                        break;
                    case NONE:
                        break;
                }
            }
            if (LOG) {
                System.out.println("------drawable-------");
                for (File file : resourcePath.drawableDir) {
                    System.out.println(file.getAbsolutePath());
                }

                System.out.println("------mipmap-------");
                for (File file : resourcePath.mipmapDir) {
                    System.out.println(file.getAbsolutePath());
                }

                System.out.println("------layout-------");
                for (File file : resourcePath.layoutDir) {
                    System.out.println(file.getAbsolutePath());
                }

                System.out.println("------string-------");
                for (File file : resourcePath.stringDir) {
                    System.out.println(file.getAbsolutePath());
                }

                System.out.println("------drawables-------");
                System.out.println(resourcePath.drawablesFile.getAbsolutePath());

                System.out.println("------public-------");
                System.out.println(resourcePath.publicFile.getAbsolutePath());
            }
            switch (type) {
                case DRAWABLE:
                    //需要改所有drawable目录下的文件
                    //同时修改所有引用到这个文件的xml
                    //最后修改pubic.xml
                    type.replace(resourcePath, prefix, targetPrefix);
                    break;
                case STRING:
                    break;
                case MIPMAP:
                    break;
                case LAYOUT:
                    break;
            }
        }
    }
}
