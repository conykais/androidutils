package com.replacestring.string_script.res.engine;

import com.replacestring.string_script.res.ResourcePath;

public interface ReplaceEngine {

    void replacePrefix(ResourcePath resourcePath, String prefix, String targetPrefix);

    void replaceName(ResourcePath resourcePath, String originName, String targetName);
}
