package com.replacestring.string_script.res;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ResourcePath {
    public List<File> drawableDir = new ArrayList<>();
    public List<File> mipmapDir = new ArrayList<>();
    public List<File> layoutDir = new ArrayList<>();
    public List<File> stringDir = new ArrayList<>();
    public File publicFile;
    public File drawablesFile;
    public File manifestFile;
}