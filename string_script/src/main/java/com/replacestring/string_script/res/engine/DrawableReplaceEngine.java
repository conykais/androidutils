package com.replacestring.string_script.res.engine;

import com.replacestring.string_script.res.ResourcePath;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.replacestring.string_script.Utils.ResType.findResReference;
import static com.replacestring.string_script.Utils.getContent;
import static com.replacestring.string_script.Utils.saveFile;

public class DrawableReplaceEngine implements ReplaceEngine {

    @Override
    public void replacePrefix(ResourcePath resourcePath, String prefix, String targetPrefix) {
        List<File> drawableDirs = resourcePath.drawableDir;
        String PNG = ".png";
        String JPG = ".jpg";
        String XML = ".xml";
        Pattern pngPattern = Pattern.compile(String.format(Locale.US, "%s_(?<name>\\w+)%s", prefix, PNG));
        Pattern jpgPattern = Pattern.compile(String.format(Locale.US, "%s_(?<name>\\w+)%s", prefix, JPG));
        Pattern xmlPattern = Pattern.compile(String.format(Locale.US, "%s_(?<name>\\w+)%s", prefix, XML));
        //找到某个资源在各个路径的集合
        Map<String, List<File>> resMap = new HashMap<>();
        //找到某个资源被layout引用到的集合
        Map<String, List<File>> layoutReferenceMap = new HashMap<>();
        //找到某个资源被drawable应用的集合
        Map<String, List<File>> drawableReferenceMap = new HashMap<>();
        if (drawableDirs != null) {
            String name;
            String originName;
            String targetName;
            String resName;
            String content;
            Matcher matcher;
            String manifestContent = null;
            String publicContent = null;
            //找到所有前缀是{prefix}的drawable [png,jpg,xml]
            for (File drawableDir : drawableDirs) {
                if (drawableDir.isDirectory()) {
                    File[] drawables = drawableDir.listFiles();
                    if (drawables != null && drawables.length > 0) {
                        for (File drawable : drawables) {
                            name = drawable.getName();
                            matcher = pngPattern.matcher(name);
                            boolean flag = false;
                            if (matcher.find()) {
                                flag = true;
                            }
                            if (!flag) {
                                matcher = jpgPattern.matcher(name);
                                if (matcher.find()) {
                                    flag = true;
                                }
                            }
                            if (!flag) {
                                matcher = xmlPattern.matcher(name);
                                if (matcher.find()) {
                                    flag = true;
                                }
                            }
                            if (flag) {
                                name = matcher.group("name");
                                resName = String.format("%s_%s", prefix, name);
                                if (!resMap.containsKey(resName)) {
                                    List<File> list = new ArrayList<>();
                                    resMap.put(resName, list);
                                    list.add(drawable);
                                } else {
                                    resMap.get(resName).add(drawable);
                                }
                            }
                        }
                    }
                }
            }

            List<File> layoutDirs = resourcePath.layoutDir;
            Set<String> keySet = resMap.keySet();
            for (String key : keySet) {
                //layout reference
                findResReference(key, "drawable", layoutDirs, layoutReferenceMap);
                //drawable.xml reference
                findResReference(key, "drawable", drawableDirs, drawableReferenceMap);
            }

            StringBuilder builder = new StringBuilder();
            StringBuilder publicBuilder = new StringBuilder();
            StringBuilder manifestBuilder = new StringBuilder();
            manifestBuilder.append("AndroidManifest :\n");
            publicBuilder.append("public :\n");
            builder.append("drawable:\n");

            for (String key : keySet) {
                originName = key;
                targetName = String.format("%s%s", targetPrefix, key.substring(prefix.length()));
                String originReference = String.format("@drawable/%s", originName);
                String targetReference = String.format("@drawable/%s", targetName);
                //noinspection ConstantConditions
                if (key != null && key.length() > 0) {
                    List<File> res = resMap.get(key);
                    if (res != null) {
                        for (File drawable : res) {
                            //rename
                            boolean result = drawable.renameTo(new File(drawable.getParentFile(), drawable.getName().replace(originName, targetName)));
                            if (result) {
                                builder.append(String.format("\t\t%s -> %s\n", originName, targetName));
                            }
                            //public.xml reference
                            if (resourcePath.publicFile != null) {
                                publicContent = getContent(resourcePath.publicFile);
                                String publicOriginName = String.format("type=\"drawable\" name=\"%s\"", originName);
                                String publicTargetName = String.format("type=\"drawable\" name=\"%s\"", targetName);
                                if (publicContent != null && publicContent.contains(publicOriginName)) {
                                    publicContent = publicContent.replace(publicOriginName, publicTargetName);
                                    result = saveFile(publicContent, resourcePath.publicFile);
                                    if (result){
                                        publicBuilder.append(String.format("\t\t%s -> %s\n", originName, targetName));
                                    }
                                }
                            }
                            //layout reference
                            List<File> layouts = layoutReferenceMap.get(key);
                            if (layouts != null) {
                                for (File layout : layouts) {
                                    content = getContent(layout);
                                    if (content != null && content.contains(originReference)) {
                                        content = content.replaceAll(originReference, targetReference);
                                        result = saveFile(content, layout);
                                        if (result) {
                                            builder.append(String.format("\t\t\t\t└──%s/%s\n", layout.getParentFile().getName(), layout.getName()));
                                        }
                                    }
                                }
                                builder.append("\n");
                            }
                            //drawable reference
                            List<File> drawables = drawableReferenceMap.get(key);
                            if (drawables != null) {
                                for (File file : drawables) {
                                    content = getContent(file);
                                    if (content != null && content.contains(originReference)) {
                                        content = content.replaceAll(originReference, targetReference);
                                        result = saveFile(content, file);
                                        if (result) {
                                            builder.append(String.format("\t\t\t\t└──%s/%s\n", file.getParentFile().getName(), file.getName()));
                                        }
                                    }
                                }
                                builder.append("\n");
                            }
                            //AndroidManifest.xml reference
                            if (resourcePath.manifestFile != null) {
                                manifestContent = getContent(resourcePath.manifestFile);
                                if (manifestContent != null && manifestContent.contains(originReference)) {
                                    manifestContent = manifestContent.replaceAll(originReference, targetReference);
                                    result = saveFile(manifestContent, resourcePath.manifestFile);
                                    if (result){
                                        manifestBuilder.append(String.format("\t\t%s -> %s\n", originName, targetName));
                                    }
                                }
                            }

                        }
                    }
                }
            }

            System.out.println(builder.toString());
            if (publicContent != null) {
                //saveFile(publicContent, resourcePath.publicFile);
                System.out.println(publicBuilder.toString());
            }
            if (manifestContent != null) {
                //saveFile(manifestContent, resourcePath.manifestFile);
                System.out.println(manifestBuilder.toString());
            }
        }
    }

    @Override
    public void replaceName(ResourcePath resourcePath, String originName, String targetName) {

    }
}
